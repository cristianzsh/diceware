# diceware
Geração de senhas seguras com o método Diceware

Diceware é um método para criar senhas e outras variáveis criptográficas usando um dado simples para gerar números aleatórios.
Seu funcionamento é o seguinte: para cada palavra na frase-senha, deve-se jogar um dado cinco vezes e anotar os números
sorteados, por exemplo: 36241. Esse número é utilizado para procurar a palavra correspondente na lista, no caso: "lease".

![diceware](http://i.imgur.com/FEF34PR.png)

Quanto mais termos você escolher para sua senha, mais forte ela ficará. Listas de palavras foram traduzidas para várias 
linguagens, Espanhol e Alemão são exemplos.
