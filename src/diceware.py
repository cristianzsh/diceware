'''
    File name: diceware.py
    Author: Cristian Henrique
    Date created: 31/03/2016
    Python Version: 2.7
'''
from random import randint

def gerar_codigo():
    gerado = ''

    for i in range(5):
        gerado += str(randint(1, 6))
    return gerado

def gerar_palavra(n):
    lista = open('diceware_word_list.asc')
    palavra = ''
    
    for linha in lista:
        if n in linha:
            palavra += linha.split()[1]
            return palavra

numero_palavras = int(input('Digite a quantidade de termos desejados para a senha: '))
frase = ''
for i in range(numero_palavras):
    n = gerar_codigo()
    frase += gerar_palavra(n)

print ("Senha gerada: %s" %frase)
